# Q-Learning (Adaptado)
## Sobre o projeto:
Este projeto, insere-se na disciplina de Inteligência Artificial, cujo tema Q-Learning (adaptado) permite dotar um agente com a capacidade de fazer uma exploração autónoma de determinado ambiente, possibilitando este a aprender os melhores percursos até determinado objetivo. 
O ambiente consiste num tabuleiro retangular de casas quadradas, que podem conter obstáculos, perigos (bombas e targets) ou objetivos. O agente executa movimentos aleatórios e pode deslocar-se em frente ou mudar de direção (N S W E). 
À medida que o agente explora o ambiente e muda de posição, e-lhe fornecido um valor de retorno. Este valor de retorno é essencial para aplicar ao quadrante do estado atual determinado valor, proveninente da aplicação da fórmula Q(s,a) =  r + Ɣ * Q’(s’,a’).
O objetivo principal é obter o maior número de informações possiveis do ambiente, por forma a que o aprimoramento da tarefa a ser executada seja conseguido à medida são executados os episódios.
A interação entre o agente e o ambiente é comandada através de um cliente e acontece no servidor.

## Como instalar
Para correr o servidor e o cliente, o utilizador deve ter instalada a versão 3 do Python. Além do Python 3, o cliente necessita da biblioteca Pillow
  
### Instalar o Python 3:
 **Nota: para facilitar a utilização no Windows, o Python deve ser adicionado ao PATH**  
- Windows: https://docs.python.org/3/using/windows.html  
- Mac: https://docs.python.org/3/using/mac.html  
- Linux: https://docs.python.org/3/using/unix.html#on-linux  
  
### Instalar a biblioteca Pillow:
  Após instalar o Python, executar na linha de comandos:  
    ```python3 -m pip install --upgrade pip```  
    ```python3 -m pip install --upgrade Pillow```  

## Como correr:
### Para correr o servidor:  
Na linha de comandos, **a partir do diretório principal do projeto**, executar:  
#### Linux:
```python3 server/main.py```
#### Windows
```python server/main.py```
  
### Para correr o cliente:  
Na linha de comandos, **a partir do diretório principal do projeto**, executar:
#### Linux:
```python3 client/client.py```
#### Windows
```python client/client.py```

### Para correr um agente:  
Na linha de comandos, **a partir do diretório principal do projeto**, executar, por exemplo:
#### Linux:
```python3 client/RL.py``` 
#### Windows
```python client/RL.py```

## Como configurar / Testes
A configuração do ambiente e do agente é feita nos diversos ficheiros json abaixo indicados, com as configurações do mapa que foram utilizados como testes.
 - config.json
 - config2.json
 - config3.json
  
## Contribuidores:
 - Bárbara Galama
 - Diogo Figueiredo
