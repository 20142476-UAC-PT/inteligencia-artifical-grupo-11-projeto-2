import client
import ast
import random
import math


class Node:
    def __init__(self, state):
        self.state = state
        self.incomingDirection = ""
        self.vNorth = 0
        self.vSouth = 0
        self.vWest = 0
        self.vEast = 0
        self.q = 0

    def getQ(self):
        return self.q

    def setQ(self, value):
        self.q = value

    def setIncomingDirection(self, dir):
        self.incomingDirection = dir

    def getState(self):
        return self.state

    def getDirection(self):
        return self.incomingDirection

    def setVNorth(self, value):
        self.vNorth += value

    def getVNorth(self):
        return self.vNorth

    def setVSouth(self, value):
        self.vSouth = self.vSouth + value

    def getVSouth(self):
        return self.vSouth

    def setVWest(self, value):
        self.vWest += value

    def getVWest(self):
        return self.vWest

    def setVEast(self, value):
        self.vEast += value

    def getVEast(self):
        return self.vEast

    def __str__(self):
        return "State : " + str(self.getState()) + " North : " + str(self.getVNorth()) + " South : " + str(
            self.getVSouth()) + " West : " + str(self.getVWest()) + " East : " + str(self.getVEast())


class Agent:
    def __init__(self):
        self.c = client.Client('127.0.0.1', 50001)
        self.res = self.c.connect()
        random.seed()  # To become true random, a different seed is used! (clock time)
        self.q_table = {}

        self.weightMap = []
        self.goalNodePos = (0, 0)
        self.state = (0, 0)
        self.maxCoord = (0, 0)
        self.profundidade = 0
        self.obstacles = self.getObstacles()

    def getConnection(self):
        return self.res

    def getDirection(self):
        msg = self.c.execute("info", "direction")
        dir = msg  # ast.literal_eval(msg)
        # test
        # print('Dir is:', dir)
        return dir

    def getGoalPosition(self):
        msg = self.c.execute("info", "goal")
        goal = ast.literal_eval(msg)
        # test
        print('Goal is located at:', goal)
        return goal

    def getSelfPosition(self):
        msg = self.c.execute("info", "position")
        pos = ast.literal_eval(msg)
        return pos

    def getMaxCoord(self):
        msg = self.c.execute("info", "maxcoord")
        max_coord = ast.literal_eval(msg)
        # test
        return max_coord

    def getObstacles(self):
        msg = self.c.execute("info", "obstacles")
        obst = ast.literal_eval(msg)
        # test
        return obst

    def getReward(self):
        msg = self.c.execute("info", "rewards")
        res = ast.literal_eval(msg)
        return res

    def convertReward(self, rewards):
        max_coord = self.getMaxCoord()
        rewads_dict = {}
        for y in range(max_coord[1]):
            for x in range(max_coord[0]):
                rewads_dict[str((x, y))] = rewards[x][y]
        return rewads_dict

    def getR(self, state):
        objects = self.getReward()
        rewards = self.convertReward(objects)
        return rewards.get(state)

    def setArrow(self, node):
        x = node.getState()[0]
        y = node.getState()[1]
        action = max((
            ("north", node.getVNorth()),
            ("south", node.getVSouth()),
            ("east", node.getVEast()),
            ("west", node.getVWest())), key=lambda _: _[1])[0]

        if max(node.getVNorth(),node.getVSouth(),node.getVEast(), node.getVWest()) > 0:
            self.c.execute("marrow", str(action + "," + str(y) + "," + str(x)))

    def run(self):
        episodes = 250
        counter = 1
        episode_array = []
        end = False
        # # Get the position of the Goal
        self.goalNodePos = self.getGoalPosition()
        # # Get the initial position of the agent
        self.state = self.getSelfPosition()
        msg = self.c.execute("command", "set_steps")

        while counter <= episodes:

            end = False
            while not end:

                msg = self.c.execute("info", "view")
                objects = ast.literal_eval(msg)

                if 'obstacle' in objects:  # or 'bomb' in objects
                    res = random.randint(1, 2)
                    if res == 1:
                        self.c.execute("command", "left")
                    else:
                        self.c.execute("command", "right")
                else:
                    res = random.randint(1, 8)
                    if 3 <= res <= 6:

                        #Obtendo direção e adionando ao array do episodio
                        dir = self.c.execute("info", "direction")
                        node = Node(self.getSelfPosition())
                        node.setIncomingDirection(dir)
                        episode_array.append(node)

                        # Adicionado ao Dicionario de estados
                        if str(node.getState()) not in self.q_table.keys():
                            self.q_table[str(node.getState())] = node

                        self.c.execute("command", "forward")

                        if 'goal' in objects or 'target' in objects:
                            episode_array.reverse()
                            if 'goal' in objects:
                                qLast = self.getR(str(self.goalNodePos))
                            if 'target' in objects:
                                qLast = -50

                            for node in episode_array:
                                # print("Invertido", node)
                                state = str(node.getState())
                                r = self.getR(state)

                                q = r + (0.9 * qLast)

                                if state in self.q_table.keys():
                                    nodeToUpdate = self.q_table[state]
                                    nodeToUpdate.setQ(round(q))

                                    # Aplicando o Q'(s',a') no quadrante correto
                                    if node.getDirection() == "north":
                                        nodeToUpdate.setVNorth(round(q))
                                    elif node.getDirection() == "south":
                                        nodeToUpdate.setVSouth(round(q))
                                    elif node.getDirection() == "west":
                                        nodeToUpdate.setVWest(round(q))
                                    elif node.getDirection() == "east":
                                        nodeToUpdate.setVEast(round(q))

                                # get last Q'
                                lastState = state
                                if str(lastState) in self.q_table.keys():
                                    lastNode = self.q_table[lastState]
                                    qLast = lastNode.getQ()

                            # Q Table
                            if counter == episodes or counter == 5 or counter == 50 or counter == 100 or counter == 200:
                                print("Episódio : ", counter)
                                for node in self.q_table.values():
                                    print(node)
                                    self.setArrow(node)

                            end = True
                            self.c.execute("command", "home")
                            episode_array = []

                    else:
                        if res <= 2:
                            self.c.execute("command", "right")
                        if res >= 7:
                            self.c.execute("command", "left")

            counter += 1


def main():
    print("Starting client!")

    ag = Agent()
    if ag.getConnection() != -1:
        ag.run()

    input("Waiting for return!")


main()
